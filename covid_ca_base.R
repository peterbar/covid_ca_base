### Code to prepare base data for the COVID-19 Canada dashboard ###

# load packages
library("tidyverse")
library("lubridate")
library("sf")
library("cansim")


## Get population estimates, by province & Canada total
population <- get_cansim("17-10-0009") %>% #, refresh = TRUE) %>% 
  filter(REF_DATE >= "2020-01") %>% 
  select(date = REF_DATE, region = GEO, population = VALUE) %>% 
  mutate(date = ymd(date, truncated = 1)) %>% 
  mutate(quarter = quarter(date, with_year = TRUE)) %>% 
  select(-date) %>% 
  relocate(quarter)

current_quarter <- quarter(Sys.Date(), with_year = TRUE)

if (current_quarter != max(population$quarter)) {
  
  # Replicate population estimates for the latest quarter from the previous quarter
  last_quarter <- tibble(quarter = rep(current_quarter, 14), # Change YYYY.Q accordingly
                         region = unique(population$region),
                         population = tail(population$population, 14))
  
  population <- union(population, last_quarter)
  
}

# Save for use by a Shiny app
write_csv(population, "population.csv")


## Get spatial data ##

# Download StatCan data for provincial boundaries
download.file(url = "http://www12.statcan.gc.ca/census-recensement/2011/geo/bound-limit/files-fichiers/2016/lpr_000b16a_e.zip",
              destfile = "provinces.zip")
unzip("provinces.zip", exdir = "provinces")
list.files("provinces/")

# Read data, drop French names, simplify to reduce size
provinces <- st_read("provinces/lpr_000b16a_e.shp",
                     stringsAsFactors = FALSE) %>% 
  select(region = PRNAME) %>% 
  mutate(region = str_remove(region, " /.*")) %>% 
  st_simplify(dTolerance = 5000) %>% # threshold in units used in the object (meters)
  st_transform(crs = 4326) %>% #*
  arrange(region) #**
#* Leaflet can only take data in WGS84 (aka EPSG:4326).
#  Polygons will then be reprojected inside Leaflet to LAEA
#  https://en.wikipedia.org/wiki/Lambert_azimuthal_equal-area_projection
#  centered on the geographic center of Canada as per Canadian Cartographic Association.
#  Note also that StatCan officially recommends LCC:
#  https://en.wikipedia.org/wiki/Lambert_conformal_conic_projection
#  but I prefer the look of an equal-area projection.
#  StatCan proj4string: "+proj=lcc +lat_1=49 +lat_2=77 +lon_0=-91.52 +x_0=0 +y_0=0 +datum=NAD83 +units=m +no_defs".
#  My proj4string: "+proj=laea +x_0=0 +y_0=0 +lon_0=-97 +lat_0=62.3 +units=m".
#
#** Making sure that 'covid$region' and 'provinces$region' are in the same order,
#   else Leaflet will incorrectly map regions to polygons.
#   Caused by changes to right_join() in dplyr 1.0.0: 
#   'right_join() no longer sorts the rows of the resulting tibble according to the order of the RHS by argument in tibble y.'


# Save for use by the Shiny app
if (file.exists("provinces.gpkg")) { file.remove("provinces.gpkg") } # st_write can't overwrite files on disk
st_write(provinces, "provinces.gpkg")
